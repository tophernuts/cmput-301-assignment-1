package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class NoteFragment extends ListFragment {

	private UpdateCallback mCallback;
	
	public static final String TITLE = "Notes";
	private static final String TAG = "NoteFragment";

	private static final boolean DEBUG_LOG_FLAG = false;

	private static NotesDataSource dataSource;
	private static List<Note> values;
	private static ArrayAdapter<Note> adapter;

	public interface UpdateCallback {
		public void updateFragments();
	}
	
	@Override
	public void onAttach(Activity a){
		super.onAttach(a);
		try{
			mCallback = (UpdateCallback) a;
		} catch(ClassCastException e){
			Log.e(TAG, "Casting calling activity as callbacks listener failed", e);
			mCallback = null;
		}
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		setHasOptionsMenu(true);
		View view = inflater.inflate(R.layout.activity_notes, container, false);

		dataSource = new NotesDataSource(getActivity());
		dataSource.open();

		values = dataSource.getAllNotes();
		adapter = new NotesListViewAdapter(getActivity(),
				R.layout.note_list_item, values);
		setListAdapter(adapter);

		registerForContextMenu(view);

		return view;

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, view, menuInfo);
		AdapterView.AdapterContextMenuInfo info;
		try {
			info = (AdapterView.AdapterContextMenuInfo) menuInfo;
		} catch (ClassCastException e) {
			Log.e(TAG, "Bad menuInfo.", e);
			return;
		}
		// TODO: Why does this have to be here to work? And why does this work?
		if (info == null) {
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "info is null.");
			return;
		}
		Note n = (Note) adapter.getItem(info.position);
		if (n == null)
			return;

		MenuInflater inflater = getActivity().getMenuInflater();
		inflater.inflate(R.menu.list_item_context, menu);
		menu.setHeaderTitle(n.getTitle());
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		AdapterView.AdapterContextMenuInfo info;
		try {
			info = (AdapterContextMenuInfo) item.getMenuInfo();
		} catch (ClassCastException e) {
			Log.e(TAG, "Bad menuinfo.", e);
			return false;
		}

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, "Hit a context menu.");
		if (info == null) {
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Info is null");
			return super.onContextItemSelected(item);
		}
		long id = adapter.getItem(info.position).getId();

		switch (item.getItemId()) {
		case R.id.list_item_context_open:
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Open context menu.");
			startActivityForResult(getNoteIntent(id),
					MainActivity.NOTE_CREATOR_REQUEST_CODE);
			return true;
		case R.id.list_item_context_delete:
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Delete note context menu.");
			dataSource.deleteNoteFromId(id);
			// TODO: Update me and the other fragments
			if(mCallback != null){
				mCallback.updateFragments();
			}
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	/**
	 * @return the Intent to start the NoteCreator activity with a new Note.
	 */
	private Intent getNewNoteIntent() {
		return getNoteIntent(NoteCreator.NEW_NOTE_ID);
	}

	/**
	 * Helper function designed to create all of the intents to start the
	 * NoteCreator activity.
	 * <p>
	 * All Intents to start the NoteCreator activity should be made by calling
	 * this method.
	 * 
	 * @param id
	 *            The id of the note to be loaded into NoteCreator. If you are
	 *            creating a new Note use {@link NoteCreator.NEW_NOTE_ID}
	 * @return the Intent to start the NoteCreator activity with
	 * */
	private Intent getNoteIntent(long id) {
		Intent i = new Intent(getActivity(), NoteCreator.class);
		i.putExtra(NoteCreator.NOTE_ID, id);
		return i;
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id) {
		long noteId = adapter.getItem(position).getId();
		if (DEBUG_LOG_FLAG)
			Log.d(TAG, String.format("Hit item with id: %d", noteId));
		startActivityForResult(getNoteIntent(noteId),
				MainActivity.NOTE_CREATOR_REQUEST_CODE);
		super.onListItemClick(l, v, position, id);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		registerForContextMenu(getListView());
	}

	@Override
	public void onResume() {
		dataSource.open();
		super.onResume();
	}

	@Override
	public void onPause() {
		dataSource.close();
		super.onPause();
	}


	/**
	 * Updates listview to acknowledge new notes from db Should only be called
	 * when you want to update the List of Notes
	 */
	public void updateNotes() {
		List<Note> newList = dataSource.getAllNotes();
		// TODO: See if I don't have to add all of the notes
		// XXX: Holy make this actually nice
		values = newList;
		adapter.clear();
		adapter.addAll(values);
		adapter.notifyDataSetChanged();

	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		inflater.inflate(R.menu.notes, menu);
		// TODO: Find a nicer way besides that dirty hack to add the add button
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.notes_action_add_note:
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Creating note...");
			startActivityForResult(getNewNoteIntent(),
					MainActivity.NOTE_CREATOR_REQUEST_CODE);

			values = dataSource.getAllNotes();
			adapter.notifyDataSetChanged();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
