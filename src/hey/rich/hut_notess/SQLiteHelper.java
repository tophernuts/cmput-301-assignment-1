package hey.rich.hut_notess;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {
	public static final String TABLE_NOTES = "notes";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NOTES = "note";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_WORD_COUNT = "num_words";
	public static final String COLUMN_CHARACTER_COUNT = "num_chars";
	public static final String COLUMN_CREATE_DATE = "create_date";
	public static final String COLUMN_MODIFY_DATE = "modify_date";

	private static final String DATABASE_NAME = "notes.db";
	private static final int DATABASE_VERSION = 1;

	private static final String DATABASE_CREATE = "create table " + TABLE_NOTES
			+ "(" + COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_TITLE + " text not null, " + COLUMN_NOTES
			+ " text not null, " + COLUMN_WORD_COUNT + " integer not null, "
			+ COLUMN_CHARACTER_COUNT + " integer not null, "
			+ COLUMN_CREATE_DATE + " integer not null, " + COLUMN_MODIFY_DATE
			+ " integer not null);";

	private static SQLiteHelper sInstance = null;

	/**
	 * Modified from:
	 * http://www.androiddesignpatterns.com/2012/05/correctly-managing
	 * -your-sqlite-database.html
	 * */
	public static SQLiteHelper getInstance(Context context) {
		if (sInstance == null) {
			sInstance = new SQLiteHelper(context.getApplicationContext());
		}
		return sInstance;
	}

	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.d(SQLiteOpenHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NOTES);
		onCreate(db);
	}
}
