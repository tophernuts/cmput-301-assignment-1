package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.GridView;

public class WordMapFragment extends Fragment {
	public static final String TITLE = "Word map";

	private GridView mGridView;

	private static NotesDataSource dataSource;

	private static Map<String, Integer> mTop20Words;
	private static List<WordFrequency> words;

	private static ArrayAdapter<WordFrequency> adapter;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle SavedInstanceState) {
		View v = inflater.inflate(R.layout.activity_word_cloud, container,
				false);

		mTop20Words = new LinkedHashMap<String, Integer>();

		mGridView = (GridView) v.findViewById(R.id.word_cloud_gridview);
		words = new ArrayList<WordFrequency>();
		adapter = new WordMapGridViewAdapter(v.getContext(),
				R.layout.word_map_item, words);
		mGridView.setAdapter(adapter);

		updateWordMap();

		return v;
	}

	public class WordFrequency {
		public WordFrequency(String s, Integer i, Integer l) {
			this.word = s;
			this.frequency = i;
			this.largest = l;
		}

		String word;
		Integer frequency;
		Integer largest;

		public Integer getFrequency() {
			return this.frequency;
		}

		public String getWord() {
			return this.word;
		}

		public Integer getLargest() {
			return this.largest;
		}
	}

	/**
	 * The asynctask can be called before our view is created, must make sure
	 * that everything exists at this time.
	 */
	private void loadGridViewAdapters() {
		if (dataSource == null) {
			dataSource = new NotesDataSource(getActivity());
		}
		if (mTop20Words == null) {
			mTop20Words = new LinkedHashMap<String, Integer>();
		}

		if (words == null) {
			words = new ArrayList<WordFrequency>();
		}

	}

	private class UpdateWordCloudTask extends AsyncTask<Void, Void, Void> {
		protected Void doInBackground(Void... voids) {
			// Update the list of words
			if (mTop20Words == null)
				mTop20Words = new LinkedHashMap<String, Integer>();
			if (dataSource == null)
				dataSource = new NotesDataSource(getActivity());

			mTop20Words = dataSource.getCommonWords(20);

			if (words != null) {
				words.clear();
			}

			Iterator<Entry<String, Integer>> it = mTop20Words.entrySet()
					.iterator();
			Map.Entry<String, Integer> pair;
			if (mTop20Words == null || mTop20Words.isEmpty()) {
				return null;
			}
			Integer largest = mTop20Words.values().iterator().next();
			while (it.hasNext()) {
				pair = (Map.Entry<String, Integer>) it.next();
				words.add(new WordFrequency(pair.getKey(), pair.getValue(),
						largest));
			}

			return null;
		}

		protected void onPreExecute() {
			// Display spinnning thing
			loadGridViewAdapters();
		}

		protected void onPostExecute(Void v) {
			// Notify adapter we've changed
			adapter.notifyDataSetChanged();

			// Get rid of spinning thing
		}
	}

	/** Updates the wordMap */
	public void updateWordMap() {
		new UpdateWordCloudTask().execute();
	}
}
