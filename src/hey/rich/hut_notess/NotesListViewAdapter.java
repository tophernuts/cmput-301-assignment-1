package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class NotesListViewAdapter extends ArrayAdapter<Note> {
	private Context c;

	public NotesListViewAdapter(Context c, int resId, List<Note> items) {
		super(c, resId, items);
		this.c = c;
	}

	private class ViewHolder {
		TextView title;
		TextView contents;
		TextView modifyDate;
		TextView wordCount;
		
	}

	public View getView(int pos, View cView, ViewGroup parent) {
		ViewHolder holder = null;
		Note n = (Note) getItem(pos);

		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (cView == null) {
			cView = inflater.inflate(R.layout.note_list_item, null);
			holder = new ViewHolder();
			holder.title = (TextView) cView.findViewById(R.id.list_note_title);
			holder.contents = (TextView) cView
					.findViewById(R.id.list_note_contents);
			holder.modifyDate = (TextView) cView
					.findViewById(R.id.list_note_date);
			holder.wordCount = (TextView) cView
					.findViewById(R.id.list_note_word_count);
			cView.setTag(holder);
		} else {
			holder = (ViewHolder) cView.getTag();
		}

		holder.title.setText(n.getTitle());
		holder.contents.setText(n.getContents());
		holder.modifyDate.setText(n.getModifiedDateString());
		holder.wordCount.setText("# of words: " + n.getWordCount());

		return cView;
	}
}
