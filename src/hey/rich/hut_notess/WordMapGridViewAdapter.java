package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;
import hey.rich.hut_notess.WordMapFragment.WordFrequency;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class WordMapGridViewAdapter extends ArrayAdapter<WordFrequency> {

	private Context c;
	private static final String TAG = "WordMapGridViewAdapter";

	public WordMapGridViewAdapter(Context c, int resId, List<WordFrequency> s) {
		super(c, resId, s);
		this.c = c;
	}

	private class ViewHolder {
		TextView word;
	}

	public View getView(int pos, View cView, ViewGroup parent) {
		ViewHolder h = null;
		WordFrequency w = (WordFrequency) getItem(pos);

		LayoutInflater inflater = (LayoutInflater) c
				.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		if (cView == null) {
			cView = inflater.inflate(R.layout.word_map_item, null);
			h = new ViewHolder();
			h.word = (TextView) cView.findViewById(R.id.grid_word_cloud_item);
			cView.setTag(h);
		} else {
			h = (ViewHolder) cView.getTag();
		}

		int size = getTextSizeFromWordFrequency(w);

		Log.d(TAG, String.format("Freq: %d size: %d", w.getFrequency(), size));
		h.word.setText(Html.fromHtml(String.format("<h%d>%s: %d</h%d>", size,
				w.getWord(), w.getFrequency(), size)));

		return cView;
	}

	private static int getTextSizeFromWordFrequency(WordFrequency wf) {
		float largest, freq;
		largest = wf.getLargest();
		freq = wf.getFrequency();
		if (freq == largest) {
			return 1;
		} else {
			float f = freq / largest * 6.0f;
			return (int) (6-f);
		}

	}

}
