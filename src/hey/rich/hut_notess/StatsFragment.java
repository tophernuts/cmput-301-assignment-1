package hey.rich.hut_notess;

import hey.rich.constants.Static_Constants;
import hey.rich.hut_assignment_1.R;

import java.util.LinkedHashMap;
import java.util.Map;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class StatsFragment extends Fragment {
	public static final String TITLE = "Stats";
	private static final String TAG = "StatsFragment";

	private static final boolean DEBUG_LOG_TAG = false;
	
	private static TextView mTVNumNotes, mTVNotesToday, mTVNumWords,
			mTVNumChar, mTVLongestNoteWord, mTVLongestNoteChar, mTVAvgNoteWord,
			mTVAvgNoteChar, mTVList100Words;

	private static NotesDataSource dataSource;

	private static int mNumNotes, mNumNotesToday, mNumWords, mNumChars,
			mLongestNoteWord, mLongestNoteChar, mAvgNoteWord, mAvgNoteChar;

	private static Map<String, Integer> mTop100Words;
	
	private static ProgressDialog mPD;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.activity_stats, container, false);

		mTVNumNotes = (TextView) v.findViewById(R.id.stats_total_num_notes);
		mTVNotesToday = (TextView) v
				.findViewById(R.id.stats_total_num_notes_today);
		mTVNumWords = (TextView) v.findViewById(R.id.stats_total_num_words);
		mTVNumChar = (TextView) v.findViewById(R.id.stats_total_num_characters);
		mTVLongestNoteWord = (TextView) v
				.findViewById(R.id.stats_longest_note_words);
		mTVLongestNoteChar = (TextView) v
				.findViewById(R.id.stats_longest_note_char);
		mTVAvgNoteWord = (TextView) v.findViewById(R.id.stats_avg_note_word);
		mTVAvgNoteChar = (TextView) v.findViewById(R.id.stats_avg_note_char);
		mTVList100Words = (TextView) v
				.findViewById(R.id.stats_list_of_top_100_words);
		

		// TODO: Consider the fact that all fragments will have their own
		// dataSource - maybe share it with magic?
		dataSource = new NotesDataSource(getActivity());

		mTop100Words = new LinkedHashMap<String, Integer>();

		// initialize ints in case to be non-"null"
		mNumNotes = mNumNotesToday = mNumWords = mNumChars = mLongestNoteWord = mLongestNoteChar = mAvgNoteWord = mAvgNoteChar = -1;

		// Update stats in case external entity has modifed DB
		update();
		return v;
	}

	public void update(){
		new UpdateStatsTask().execute();
	}
	
	private class UpdateStatsTask extends AsyncTask<Void, Void, Void>{
		protected Void doInBackground(Void... voids){
			
			// Update the stats values
			updateStats();
			
			return null;
		}
		
		protected void onPreExecute(){
			// Display spinning circle thing
			
		}
		
		protected void onPostExecute(Void v){
			// Update the UI
			updateViewsIfChanged();
			
			// Get rid of spinning thing
		}
	}
	
	/**
	 * Will update all of the stats.
	 * 
	 * TODO: Call this from the DB/NoteCreator when the db has been updated
	 */
	private static void updateStats() {
		// Update Number of notes
		mNumNotes = dataSource.getNumberOfNotes();
		
		// Update Number of notes made today
		mNumNotesToday = dataSource.getNumNotesToday();
		
		// Update Number or words
		mNumWords = dataSource.getTotalNumberOfWords();

		// Update Number of chars
		mNumChars = dataSource.getTotalNumberOfCharacters();

		// Update Longest note by words
		mLongestNoteWord = dataSource.getMaxWordsInANote();

		// Update Longest note by char
		mLongestNoteChar = dataSource.getMaxCharsInANote();

		// Update avg note by word
		mAvgNoteWord = dataSource.getAvgWordInNote();
		// Update avg note by char
		mAvgNoteChar = dataSource.getAvgCharInNote();

		// Update list of 100 best words
		mTop100Words = dataSource.getCommonWords(100);

	}

	/** Will update all of the textviews if the corresponding value has changed */
	private static void updateViewsIfChanged() {
		// TODO: are the checks really necessary? I don't think they'll provide
		// much overhead

		// Check if number of notes has changed
		mTVNumNotes.setText(Static_Constants.NUM_NOTES_STRING + mNumNotes);

		// Check if number of notes today has changed
		mTVNotesToday
				.setText(Static_Constants.NUM_NOTES_TODAY + mNumNotesToday);

		// Check if total words has changed
		mTVNumWords.setText(Static_Constants.NUM_WORDS + mNumWords);

		// Check if total chars has changed
		mTVNumChar.setText(Static_Constants.NUM_CHARS + mNumChars);

		// Check if longest word note has changed
		mTVLongestNoteWord.setText(Static_Constants.LONG_NOTE_WORD
				+ mLongestNoteWord);

		// Check if longest char note has changed
		mTVLongestNoteChar.setText(Static_Constants.LONG_NOTE_CHAR
				+ mLongestNoteChar);

		// Check if avg word note has changed
		mTVAvgNoteWord.setText(Static_Constants.AVG_NOTE_WORD + mAvgNoteWord);

		// Check if avg char note has changed
		mTVAvgNoteChar.setText(Static_Constants.AVG_NOTE_CHAR + mAvgNoteChar);

		// Check if 100 note list has changed

		// TODO: Implement setting of
		StringBuilder sb = new StringBuilder(Static_Constants.LIST_100_WORDS);
		int i = 1;
		if (DEBUG_LOG_TAG)
			Log.d(TAG, String.format("Displaying %d most common words",
					mTop100Words.size()));
		for (String s : mTop100Words.keySet()) {
			sb.append("\n" + i + ": " + s + " " + mTop100Words.get(s) + " times");
			i++;
		}

		// mTVList100Words.setText(sb.toString());
		mTVList100Words.setText(sb.toString());
	}
}
