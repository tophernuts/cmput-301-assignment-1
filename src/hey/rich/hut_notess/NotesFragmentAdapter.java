package hey.rich.hut_notess;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class NotesFragmentAdapter extends FragmentPagerAdapter {

	private static final int NUMBER_OF_FRAGMENTS = 3;

	public NotesFragmentAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		// When adding additional fragments be sure to update
		// NUMBER_OF_FRAGMENTS.
		switch (position) {
		case 0:
			// Notes fragment
			return new NoteFragment();
		case 1:
			// Stats about them notes!
			return new StatsFragment();
		case 2:
			// Word map thing
			return new WordMapFragment();
		default:
			return null;
		}
	}

	@Override
	public int getCount() {
		return NUMBER_OF_FRAGMENTS;
	}

	@Override
	public CharSequence getPageTitle(int position) {
		switch (position) {
		case 0:
			return NoteFragment.TITLE;
		case 1:
			return StatsFragment.TITLE;
		case 2:
			return WordMapFragment.TITLE; 
		default:
			return null;
		}
	}
}
