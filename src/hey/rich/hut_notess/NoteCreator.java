package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;

import java.util.Calendar;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class NoteCreator extends FragmentActivity implements OnDateSetListener {
	private static final String TAG = "NoteCreator";
	private static final boolean DEBUG_LOG_FLAG = false;

	public static final String NOTE_ID = "note_id";
	public static final long NEW_NOTE_ID = -4815162342L;

	// TODO: Add functionality to read only notes
	public static final String READ_ONLY = "read_note";

	/**
	 * Flag to update DB. 0 - check if db has changed then setResult accordingly
	 * -1 - don't even check if db has changed, just return nothing happened 1 -
	 * don't check if db has changed, just return that we changed the db.
	 */
	private static int updateDB = 0;

	EditText mTitleText, mNoteText;
	TextView mNoteCreateDate, mNoteModifyDate, mNoteWordCount, mNoteCharCount;

	private Note mNote;

	private NotesDataSource dataSource;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_note_creator);

		Bundle extras = getIntent().getExtras();
		long id = NEW_NOTE_ID;

		if (extras != null) {
			id = extras.getLong(NOTE_ID);
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, String.format("Got note id: %s", id));
		}

		dataSource = new NotesDataSource(this);

		mTitleText = (EditText) findViewById(R.id.editNoteTitle);
		mNoteText = (EditText) findViewById(R.id.editNote);
		mNoteCreateDate = (TextView) findViewById(R.id.noteCreateDate);
		mNoteModifyDate = (TextView) findViewById(R.id.noteModifyDate);
		mNoteWordCount = (TextView) findViewById(R.id.noteWordCount);
		mNoteCharCount = (TextView) findViewById(R.id.noteCharCount);

		setNoteFromId(id);

	}

	/**
	 * @param id
	 *            The id of the note to load from the database. If id is
	 *            {@link NEW_NOTE_ID} then {@link mNote} is set to null
	 */
	private void setNoteFromId(long id) {
		if (id == NEW_NOTE_ID) {
			// Creating new note
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Starting the creation of a new note.");
			mNote = null;
		} else {
			// Loading an old note
			mNote = dataSource.getNoteFromId(id);

			if (mNote != null) {
				// Successfully got the note from the database
				loadNoteContents();
			}
		}
	}

	/**
	 * Helper method that loads the existing contents of a note into the
	 * NoteCreator
	 */
	private void loadNoteContents() {
		mTitleText.setText(mNote.getTitle());
		mNoteText.setText(mNote.getContents());
		// TODO: Hardcode some explaining strings in here
		mNoteCreateDate.setText("Create date: " + mNote.getCreateDateString());
		mNoteModifyDate.setText("Last Modified: "
				+ mNote.getModifiedDateString());
		mNoteWordCount.setText("Number of words: " + mNote.getWordCount());
		mNoteCharCount.setText("Number of characters: "
				+ mNote.getCharacterCount());

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, String.format(
					"Modified epoch %d, create epoch: %d",
					mNote.getModifiedDateEpoch(), mNote.getCreateDateEpoch()));
	}

	/**
	 * Updates all of {@link mNote}'s info except for its id.
	 */
	private void updateNoteInfo() {
		mNote.setTitle(mTitleText.getText().toString());
		mNote.setContents(mNoteText.getText().toString());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.note_creator, menu);
		return true;
	}

	@Override
	public void onBackPressed(){
		super.onBackPressed();
		this.finish();
	}
	
	/**
	 * @return true iff The note has changed from its stored version in the
	 *         database
	 */
	private boolean hasNoteChanged() {
		// Check for new note
		if (mNote == null) {
			if(mTitleText.getText().toString().equals("") && mNoteText.getText().toString().equals("")){
				// New note nothing here
				if(DEBUG_LOG_FLAG) Log.d(TAG, "Title and Text are empty, no note was created.");
				return false;
			}else{
				// Either the title or note was changed so we can return true
				if(DEBUG_LOG_FLAG) Log.d(TAG, "Either title or text was changed, so we create a new note.");
				// Create the new note
				loadmNoteFromText();
				dataSource.createNote(mNote);
				return true;
			}
			
		}
		if (!mNote.getTitle().equals(mTitleText.getText().toString())) {
			// Title has changed
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Title has changed, updating notes.");
			return true;
		}

		if (!mNote.getContents().equals(mNoteText.getText().toString())) {
			// Notes contents have changed
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Note contents have changed, updating notes.");
			return true;
		}
		Note cNote = dataSource.getNoteFromId(mNote.getId());
		if (cNote == null) {
			// Note is new - save it
			return true;
		}
		if (mNote.getModifiedDateEpoch() != cNote.getModifiedDateEpoch()) {
			// The dates of the two notes differ
			if (DEBUG_LOG_FLAG) {
				Log.d(TAG, "Dates of note has changed, updating note.");
				Log.d(TAG,
						String.format("Old value: %d new Value: %d",
								cNote.getModifiedDateEpoch(),
								mNote.getModifiedDateEpoch()));
			}
			return true;
		}

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, "Note has not changed, not updating anything!");
		return false;
	}

	private void updateNoteInDatabase(boolean updateTime) {
		// Check if note is any different from stored values
		if (!hasNoteChanged()) {
			// Note has not changed, no need to update DB
			return;
		}

		// Update note modify time
		if (updateTime) {
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Updating modified time to current time.");
			mNote.updateDate();
		}
		updateNoteInfo();

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, String.format(
					"Updating mNote in db with epoch being: %d",
					mNote.getModifiedDateEpoch()));
		int updated = dataSource.updateNote(mNote);
		if (DEBUG_LOG_FLAG)
			Log.d(TAG,
					String.format("Number of notes updated was: %d.", updated));

	}

	/** Creates mNote object from text elements */
	private void loadmNoteFromText() {
		mNote = new Note(4815162342L, mTitleText.getText().toString(),
				mNoteText.getText().toString());
	}

	@Override
	public void finish() {
		// Modified from:
		// http://www.vogella.com/articles/AndroidIntent/article.html#intentdatatransfer_accessdata
		// Prepare data intent
		switch (updateDB) {
		case -1:
			// Don't check just set result to no changes
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Cancelling note, not changes are made.");
			setResult(MainActivity.NOTE_CREATOR_NO_CHANGES);
			break;
		case 0:
			// check and set result accordingly
			if (hasNoteChanged()) {
				setResult(MainActivity.NOTE_CREATOR_UPDATED_DATABASE);
				notifySavedNote();
				if (DEBUG_LOG_FLAG)
					Log.d(TAG, "Found a change, setting result to updated db");
			} else {
				setResult(MainActivity.NOTE_CREATOR_NO_CHANGES);
				if (DEBUG_LOG_FLAG)
					Log.d(TAG,
							"Didn't find a change, setting result to no change");
			}
			break;
		case 1:
			// don't check, just set result to changes
			setResult(MainActivity.NOTE_CREATOR_UPDATED_DATABASE);
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Setting result to updated db");
			notifySavedNote();
			break;
		default:
			Log.i(TAG, "Invalid updateDB");
			break;
		}

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, "Note creators finish");
		super.finish();
	}

	/** Notifies user that the note was saved */
	private void notifySavedNote() {
		Toast.makeText(getApplication(), getString(R.string.note_saved),
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.save_note:
			updateDB = 1;
			if (mNote == null) {
				loadmNoteFromText();
				dataSource.createNote(mNote);
			} else {
				// Check if we actually updated the note
				updateNoteInDatabase(true);
			}
			loadNoteContents();
			notifySavedNote();
			return true;
		case R.id.cancel_note:
			// Leave the activity
			updateDB = -1;
			finish();
			return true;
		case R.id.change_note_date:
			updateDB = 1;
			if (mNote == null) {
				mNote = new Note(0, mTitleText.getText().toString(), mNoteText
						.getText().toString());
			}
			showDatePickerDialog();
			return true;
		default:
			this.finish();
			return super.onOptionsItemSelected(item);
		}

	}

	@Override
	protected void onPause() {
		dataSource.close();
		super.onPause();
	}

	@Override
	protected void onResume() {
		dataSource.open();
		super.onResume();
	}

	public void showDatePickerDialog() {
		FragmentManager fm = getSupportFragmentManager();
		TimePickerFragment newFragment = new TimePickerFragment(this);
		newFragment.show(fm, "date_picker");
	}

	@Override
	public void onDateSet(DatePicker view, int year, int month, int day) {
		if (DEBUG_LOG_FLAG)
			Log.d(TAG, "Date was just set.");
		if (mNote == null) {
			loadmNoteFromText();
		}
		Calendar d = mNote.getModifiedDate();
		d.set(year, month, day);

		if (DEBUG_LOG_FLAG)
			Log.d(TAG, String.format(
					"Setting date now, we got year: %d month: %d, and day: %d",
					year, month, day));

		mNote.setModifyDate(d);

		if (DEBUG_LOG_FLAG)
			Log.d(TAG,
					String.format("Date is now: %s",
							mNote.getModifiedDateString()));

		if (DEBUG_LOG_FLAG)
			Log.d(TAG,
					String.format("Epoch date is now: %d",
							mNote.getModifiedDateEpoch()));

		updateNoteInDatabase(false);

		// Update note in NoteCreator
		setNoteFromId(mNote.getId());
	}

	@SuppressLint("ValidFragment")
	public class TimePickerFragment extends DialogFragment {
		private OnDateSetListener listener;

		public TimePickerFragment(OnDateSetListener listener) {
			this.listener = listener;
		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			Calendar d = mNote.getModifiedDate();

			int year = d.get(Calendar.YEAR);
			int month = d.get(Calendar.MONTH);
			int day = d.get(Calendar.DAY_OF_MONTH);

			if (DEBUG_LOG_FLAG)
				Log.d(TAG,
						String.format(
								"Starting timepicker thing with year: %d month: %d day: %d",
								year, month, day));

			return new DatePickerDialog(getActivity(), listener, year, month,
					day);
		}
	}

}
