package hey.rich.hut_notess;

import hey.rich.hut_assignment_1.R;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

public class MainActivity extends FragmentActivity implements
		NoteFragment.UpdateCallback {

	ViewPager mPager;
	NotesFragmentAdapter mAdapter;

	public static final int NOTE_CREATOR_REQUEST_CODE = 1;
	public static final int NOTE_CREATOR_UPDATED_DATABASE = 2;
	public static final int NOTE_CREATOR_NO_CHANGES = 3;

	private static final boolean DEBUG_LOG_FLAG = false;
	private static final String TAG = "MainActivity";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		mAdapter = new NotesFragmentAdapter(getSupportFragmentManager());

		mPager = (ViewPager) findViewById(R.id.pager);
		mPager.setAdapter(mAdapter);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (DEBUG_LOG_FLAG)
			Log.d(TAG, "In onActivityResult in the main activity");
		if (DEBUG_LOG_FLAG)
			Log.d(TAG, String.format("Request code: %d result code: %d",
					requestCode, resultCode));

		// Due to:
		// http://blog.tgrigsby.com/2012/04/18/android-fragment-frustration.aspx
		// Ignoring request code

		switch (resultCode) {
		case NOTE_CREATOR_UPDATED_DATABASE:
			// Update stuff
			if (DEBUG_LOG_FLAG)
				Log.d(TAG, "Got intent saying note creator updated db.");
			updateFragments();
			break;
		case NOTE_CREATOR_NO_CHANGES:
			// Do nothing.
			if (DEBUG_LOG_FLAG)
				Log.d(TAG,
						"Got intent saying note creator finished but didn't change anything");
			break;
		default:
			// Invalid result code
			break;
		}
	}

	/** Updates the fragments*/
	@Override
	public void updateFragments() {
		new NoteFragment().updateNotes();
		new WordMapFragment().updateWordMap();
		new StatsFragment().update();
	}
}
