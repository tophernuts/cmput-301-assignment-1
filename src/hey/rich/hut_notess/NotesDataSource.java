package hey.rich.hut_notess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class NotesDataSource {

	private static final String TAG = "NotesDataSource";
	private static final boolean DEBUG_FLAG = false;
	private static final boolean DEBUG_TIME_FLAG = false;

	// Database fields
	private static SQLiteDatabase database;
	private SQLiteHelper dbHelper;
	private String[] allColumns = { SQLiteHelper.COLUMN_ID,
			SQLiteHelper.COLUMN_TITLE, SQLiteHelper.COLUMN_NOTES,
			SQLiteHelper.COLUMN_WORD_COUNT,
			SQLiteHelper.COLUMN_CHARACTER_COUNT,
			SQLiteHelper.COLUMN_CREATE_DATE, SQLiteHelper.COLUMN_MODIFY_DATE };

	private String idEquals = SQLiteHelper.COLUMN_ID + " = ";

	public NotesDataSource(Context context) {
		dbHelper = SQLiteHelper.getInstance(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
		// TODO: Should I be opening and closing every time?
		// TODO: Why do my opens and close do shit with different things?
	}

	public int updateNote(Note note) {

		ContentValues values = new ContentValues();
		// TODO: Is it cheaper to compare to old values before updating it?

		// New values for the columns
		values.put(SQLiteHelper.COLUMN_TITLE, note.getTitle());
		values.put(SQLiteHelper.COLUMN_NOTES, note.getContents());
		values.put(SQLiteHelper.COLUMN_WORD_COUNT, note.getWordCount());
		values.put(SQLiteHelper.COLUMN_CHARACTER_COUNT,
				note.getCharacterCount());
		values.put(SQLiteHelper.COLUMN_CREATE_DATE, note.getCreateDateEpoch());
		values.put(SQLiteHelper.COLUMN_MODIFY_DATE, note.getModifiedDateEpoch());
		long id = note.getId();

		return database.update(SQLiteHelper.TABLE_NOTES, values,
				SQLiteHelper.COLUMN_ID + "=" + id, null);
	}

	public Note createNote(Note note) {
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_TITLE, note.getTitle());
		values.put(SQLiteHelper.COLUMN_NOTES, note.getContents());
		values.put(SQLiteHelper.COLUMN_WORD_COUNT, note.getWordCount());
		values.put(SQLiteHelper.COLUMN_CHARACTER_COUNT,
				note.getCharacterCount());
		values.put(SQLiteHelper.COLUMN_CREATE_DATE, note.getCreateDateEpoch());
		values.put(SQLiteHelper.COLUMN_MODIFY_DATE, note.getModifiedDateEpoch());
		long insertId = database.insert(SQLiteHelper.TABLE_NOTES, null, values);
		if (DEBUG_FLAG)
			Log.d(TAG,
					String.format(
							"Just created note with title %s\n content: %s\n and id: %s\n at time: %s\ninto db.",
							note.getTitle(), note.getContents(), note.getId(),
							note.getModifiedDateString()));
		Cursor cursor = database.query(SQLiteHelper.TABLE_NOTES, allColumns,
				SQLiteHelper.COLUMN_ID + " = " + insertId, null, null, null,
				null);
		cursor.moveToFirst();
		Note newNote = cursorToNote(cursor);
		closeCursor(cursor);
		return newNote;
	}

	public void deleteNote(Note note) {
		deleteNoteFromId(note.getId());
	}

	public void deleteNoteFromId(long id) {
		if (DEBUG_FLAG)
			Log.d(TAG, String.format("Note deleted with id: %d", id));
		database.delete(SQLiteHelper.TABLE_NOTES, SQLiteHelper.COLUMN_ID
				+ " = " + id, null);
	}

	/**
	 * @return The note that has the specified id
	 * @param id
	 *            The id of the note to return
	 */
	public Note getNoteFromId(long id) {
		Note n;

		if (!database.isOpen()) {
			this.open();
		}

		Cursor c = database.query(SQLiteHelper.TABLE_NOTES, allColumns,
				idEquals + id, null, null, null, null);
		c.moveToFirst();

		if (c.getCount() != 1) {
			// Didn't get the correct amount of Notes from the query
			Log.e(TAG, String.format(
					"Query for Note with id: %d didn't return only one Note.",
					id));
			return null;
		}

		// Note is good at this point
		n = cursorToNote(c);

		closeCursor(c);

		return n;
	}

	/** @return The total number of notes */
	public int getNumberOfNotes() {
		// Get all rows then count the number of them
		Cursor c = database.query(SQLiteHelper.TABLE_NOTES,
				new String[] { SQLiteHelper.COLUMN_ID }, null, null, null,
				null, null);
		int retVal = c.getCount();
		if (c != null && !c.isClosed()) {
			closeCursor(c);
		}
		return retVal;
	}

	/**
	 * @return The sum of all of the values in Column: {@link col}
	 * @param the
	 *            col that you want the sum of
	 */
	private int getSumOfColumn(String col) {
		String table;
		int retVal;
		table = SQLiteHelper.TABLE_NOTES;

		// TODO: Check if column exists - or is check at end okay?

		Cursor c = database.rawQuery(
				String.format("SELECT SUM(%s) FROM %s", col, table), null);
		if (c.moveToFirst()) {
			retVal = c.getInt(0);
		} else {
			Log.i(TAG,
					String.format(
							"Couldn't move cursor to front, column %s probably doesn't exist.",
							col));
			retVal = -1;
		}
		closeCursor(c);
		return retVal;
	}

	public int getTotalNumberOfWords() {
		return getSumOfColumn(SQLiteHelper.COLUMN_WORD_COUNT);
	}

	/** @return The total number of characters in all of the notes */
	public int getTotalNumberOfCharacters() {
		return getSumOfColumn(SQLiteHelper.COLUMN_CHARACTER_COUNT);
	}

	/** @return a list of all of the notes */
	public List<Note> getAllNotes() {
		List<Note> notes = new ArrayList<Note>();

		if (!database.isOpen()) {
			this.open();
		}
		Cursor cursor = database.query(SQLiteHelper.TABLE_NOTES, allColumns,
				null, null, null, null, SQLiteHelper.COLUMN_MODIFY_DATE
						+ " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Note n = cursorToNote(cursor);
			if (DEBUG_FLAG)
				Log.d(TAG,
						"Adding note with epoch straight from the cursor in the db: "
								+ n.getModifiedDateEpoch());
			notes.add(n);
			cursor.moveToNext();
		}
		closeCursor(cursor);
		return notes;
	}

	/** @return the largest amount of words in a single note */
	public int getMaxWordsInANote() {
		return getMaxOfColumn(SQLiteHelper.COLUMN_WORD_COUNT);
	}

	/** @return the largest amount of characters in a single note */
	public int getMaxCharsInANote() {
		return getMaxOfColumn(SQLiteHelper.COLUMN_CHARACTER_COUNT);
	}

	/**
	 * @return The max value of column: {@link col}
	 * @param col
	 *            The column to calculate the max of
	 */
	private int getMaxOfColumn(String col) {
		// TODO: Merge this with {@link getSumOfColumn}
		String table = SQLiteHelper.TABLE_NOTES;
		int retVal;

		// TODO: Check if column exists - or is latter check okay?

		Cursor c = database.rawQuery(
				String.format("SELECT MAX(%s) FROM %s", col, table), null);

		if (c.moveToFirst()) {
			retVal = c.getInt(0);
		} else {
			Log.i(TAG,
					String.format(
							"Couldn't move cursor to front, column %s probably doesn't exist",
							col));
			retVal = -1;
		}
		closeCursor(c);
		return retVal;

	}

	/** @return the average number of words in a note */
	public int getAvgWordInNote() {
		return getAvgOfCol(SQLiteHelper.COLUMN_WORD_COUNT);
	}

	/** @return the average number of characters in a note */
	public int getAvgCharInNote() {
		return getAvgOfCol(SQLiteHelper.COLUMN_CHARACTER_COUNT);
	}

	/**
	 * @return The average value of column: {@link col}
	 * @param col
	 *            The column to calculate the avg of
	 */
	private int getAvgOfCol(String col) {
		// TODO: merge this with {@link getSumOfColumn} and {@link getMaxOfCOl}
		String table = SQLiteHelper.TABLE_NOTES;
		int retVal;

		// TODO: Check if column exists - or is latter check okay?

		Cursor c = database.rawQuery(
				String.format("SELECT AVG(%s) FROM %s", col, table), null);

		if (c.moveToFirst()) {
			retVal = c.getInt(0);
		} else {
			Log.i(TAG,
					String.format(
							"Couldn't move cursor to front, column %s probably doesn't exist",
							col));
			retVal = -1;
		}
		closeCursor(c);
		return retVal;
	}

	/** @returns the number of notes that were created today. */
	public int getNumNotesToday() {
		int retVal;
		// TODO: Does this even work?
		Calendar today = Calendar.getInstance();
		int day, month, year;
		day = today.get(Calendar.DAY_OF_MONTH);
		month = today.get(Calendar.MONTH);
		year = today.get(Calendar.YEAR);

		today.set(year, month, day, 0, 0, 0);
		Calendar tomorrow = Calendar.getInstance();
		tomorrow.set(year, month, day + 1, 0, 0, 0);

		long todayEpoch, tomorrowEpoch;
		todayEpoch = today.getTimeInMillis();
		tomorrowEpoch = tomorrow.getTimeInMillis();

		if (DEBUG_TIME_FLAG)
			Log.d(TAG, String.format(
					"Looking for days between today: %d and tomorrow: %d",
					todayEpoch, tomorrowEpoch));
		String query = String.format(
				"SELECT COUNT(*) FROM %s WHERE %s <= %d AND %s >= %d",
				SQLiteHelper.TABLE_NOTES, SQLiteHelper.COLUMN_CREATE_DATE,
				tomorrowEpoch, SQLiteHelper.COLUMN_CREATE_DATE, todayEpoch);

		if (DEBUG_TIME_FLAG)
			Log.d(TAG, query);

		Cursor c = database.rawQuery(query, null);

		if (c.moveToFirst()) {
			retVal = c.getInt(0);
		} else {
			Log.i(TAG, String.format("Couldn't move cursor to front"));
			retVal = -1;
		}
		closeCursor(c);

		return retVal;
	}

	/**
	 * @return a list of the 100 most common words in notes. Modified from:
	 *         http://stackoverflow.com/a/5211215/1684866
	 * @param the
	 *            max number of words to return
	 */
	public LinkedHashMap<String, Integer> getCommonWords(int num) {
		List<String> listOfNotes = new ArrayList<String>();
		LinkedHashMap<String, Integer> map = new LinkedHashMap<String, Integer>();

		Cursor c = database.query(SQLiteHelper.TABLE_NOTES,
				new String[] { SQLiteHelper.COLUMN_NOTES }, null, null, null,
				null, null);
		if (c.moveToFirst()) {
			while (!c.isAfterLast()) {
				// fill list with all notes
				listOfNotes.add(c.getString(0));
				c.moveToNext();
			}
		} else {
			Log.i(TAG, "Couldn't get list of notes.");
			closeCursor(c);
			return map;
		}

		final List<String> listOfWords = new ArrayList<String>();
		String[] noSpaces;
		for (String s : listOfNotes) {
			// Splits and adds all words to listOfWords
			if (DEBUG_FLAG)
				Log.d(TAG, "Splitting up note: " + s);
			noSpaces = s.split(" ");
			for (String space : noSpaces) {
				if (!(space.contains(" ") || space.contains("\n") || space
						.isEmpty())) {
					listOfWords.add(space);
				}
			}
		}

		// TODO: Optimize this

		final class FrequencyComparator implements Comparator<String> {

			@Override
			public int compare(String s1, String s2) {
				if (DEBUG_FLAG)
					Log.d(TAG, String.format(
							"Comparing words: %s (%d times) and %s (%d times)",
							s1, Collections.frequency(listOfWords, s1), s2,
							Collections.frequency(listOfWords, s2)));
				return Collections.frequency(listOfWords, s2)
						- Collections.frequency(listOfWords, s1);
			}
		}

		Collections.sort(listOfWords, new FrequencyComparator());

		if (DEBUG_FLAG)
			for (String s : listOfWords) {
				Log.d(TAG, String.format("1 Word: " + s));
			}

		// Remove duplicates
		Set<String> wordSet = new LinkedHashSet<String>(listOfWords);
		if (DEBUG_FLAG)
			for (String s : wordSet) {
				Log.d(TAG, String.format("2 Word: " + s));
			}

		List<String> shortListOfWords = new ArrayList<String>();
		shortListOfWords.addAll(wordSet);

		int endPoint;
		if (shortListOfWords.size() - 1 < num) {
			endPoint = shortListOfWords.size() - 1;
		} else {
			endPoint = num;
		}

		// Make short list the correct size
		Log.d(TAG, String.format("Size of list: %d num: %d endpoint: %d",
				shortListOfWords.size(), num, endPoint));
		if (endPoint == -1) {
			return map;
		}
		shortListOfWords = shortListOfWords.subList(0, endPoint);

		for (String s : listOfWords) {
			map.put(s, Collections.frequency(listOfWords, s));
		}

		return map;

	}

	/** Utility method to nicely close cursors */
	private void closeCursor(Cursor c) {
		if (c != null && !c.isClosed()) {
			c.close();
		}
	}

	private Note cursorToNote(Cursor cursor) {
		// TODO: Change constructor to just take in epoch time?

		Note n = new Note(cursor.getLong(cursor
				.getColumnIndex(SQLiteHelper.COLUMN_ID)),
				cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.COLUMN_TITLE)),
				cursor.getString(cursor
						.getColumnIndex(SQLiteHelper.COLUMN_NOTES)),
				cursor.getLong(cursor
						.getColumnIndex(SQLiteHelper.COLUMN_CREATE_DATE)),
				cursor.getLong(cursor
						.getColumnIndex(SQLiteHelper.COLUMN_MODIFY_DATE)));

		if (DEBUG_FLAG)
			Log.d(TAG,
					String.format(
							"Creating note from db with create epoch: %d and modified epoch: %d",
							cursor.getLong(cursor
									.getColumnIndex(SQLiteHelper.COLUMN_CREATE_DATE)),
							cursor.getLong(cursor
									.getColumnIndex(SQLiteHelper.COLUMN_MODIFY_DATE))));

		return n;

	}
}
