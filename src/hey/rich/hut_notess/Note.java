package hey.rich.hut_notess;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import android.util.Log;

public class Note {

	private static final boolean DEBUG_LOG_FLAG = false;
	private static final String TAG = "NOTE";

	// Used for Date formatting
	private final static Locale DATE_LOCALE = Locale.CANADA;
	private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private Calendar mCreateDate;
	private Calendar mModifiedDate;
	private String mTitle;
	private String mContents;
	private long mId;
	private long mWordCount;
	private long mCharacterCount;

	public Note(long id, String title, String contents) {
		this(id, title, contents, Calendar.getInstance(), Calendar
				.getInstance());
	}

	public Note(long id, String title, String contents, Calendar createDate) {
		this(id, title, contents, createDate, Calendar.getInstance());
	}

	public Note(long id, String title, String contents, long createDate) {
		this(id, title, contents, createDate, Calendar.getInstance()
				.getTimeInMillis());
	}

	public Note(long id, String title, String contents, long createDate,
			long modifiedDate) {
		Calendar c = Calendar.getInstance();
		Calendar m = Calendar.getInstance();

		c.setTimeInMillis(createDate);
		m.setTimeInMillis(modifiedDate);

		if (DEBUG_LOG_FLAG)
			Log.d(TAG,
					String.format(
							"In note creating note with epoch times create: %d modify: %d",
							c.getTimeInMillis(), m.getTimeInMillis()));

		this.mCreateDate = c;
		this.mModifiedDate = m;

		// TODO: Figure out what to do instead of almost duplicate constructors

		this.mId = id;
		this.mTitle = title;
		this.mContents = contents;

		// TODO: Does word count care about title
		this.mWordCount = getWordCountFromNote(contents);
		this.mCharacterCount = contents.length();
	}

	public Note(long id, String title, String contents, Calendar createDate,
			Calendar modifiedDate) {
		this.mId = id;
		this.mTitle = title;
		this.mContents = contents;
		// TODO: Does the word count care about the title?
		this.mWordCount = getWordCountFromNote(contents);
		this.mCharacterCount = contents.length();

		this.mCreateDate = createDate;
		this.mModifiedDate = modifiedDate;
	}

	/**
	 * @return the word count of the note
	 * @param the
	 *            text of the note.
	 */
	private static int getWordCountFromNote(String note) {
		String[] words = note.split(" ");
		int wordCount = 0;

		for (String s : words) {
			if (DEBUG_LOG_FLAG)
			if (!s.contains(" ") && !s.equals("")) {
				if (DEBUG_LOG_FLAG)
				wordCount++;
			}
		}

		return wordCount;
	}

	public long getId() {
		return this.mId;
	}

	public void setId(long id) {
		this.mId = id;
	}

	public Calendar getCreateDate() {
		return this.mCreateDate;
	}

	/**
	 * Utility function that returns the date as a String using the specified
	 * format and Locale
	 * 
	 * @param date
	 *            The date to return
	 * @param format
	 *            The format of the date to return see {@link SimpleDateFormat}
	 * @return The date as a formatted string.
	 */
	public String getDateStringFromFormat(Calendar date, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format, DATE_LOCALE);
		// TODO: Maybe do this a bit nicer?
		return sdf.format(date.getTime());
	}

	public String getCreateDateString() {
		return getDateStringFromFormat(this.mCreateDate, DATE_FORMAT);
	}

	public long getCreateDateEpoch() {
		return this.mCreateDate.getTimeInMillis();
	}

	public void updateDate() {
		this.mModifiedDate = Calendar.getInstance();
	}

	public void setModifyDate(Calendar date) {
		this.mModifiedDate = date;
	}

	public String getModifiedDateString() {
		return getDateStringFromFormat(this.mModifiedDate, DATE_FORMAT);
	}

	public Calendar getModifiedDate() {
		return this.mModifiedDate;
	}

	public long getModifiedDateEpoch() {
		return this.mModifiedDate.getTimeInMillis();
	}

	public void setTitle(String title) {
		// TODO: Does the word count care about the title?
		this.mTitle = title;
	}

	public String getTitle() {
		return this.mTitle;
	}

	public void setContents(String contents) {
		this.mContents = contents;
		// TODO: Does the word count care about the title?
		this.mWordCount = contents.split(" ").length;
		this.mCharacterCount = contents.length();
	}

	public String getContents() {
		return this.mContents;
	}

	public long getWordCount() {
		return this.mWordCount;
	}

	public long getCharacterCount() {
		return this.mCharacterCount;
	}

	// was Used by ArrayAdapter in the ListView
	@Override
	public String toString() {
		return mTitle;
	}
}
