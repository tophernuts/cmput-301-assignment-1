package hey.rich.constants;

/**
 * This is a class used to hold some static constants, especially strings. There
 * is some overhead when trying to access resources from a static method, see:
 * http://stackoverflow.com/a/12506101/1684866
 * <p>
 * To improve String loading times, some constants will be stored here.
 */
public final class Static_Constants {
	public static final String NUM_NOTES_STRING = "Total Number of notes: ";
	public static final String NUM_NOTES_TODAY = "Total Number of notes created today: ";
	public static final String NUM_WORDS = "Total Number of words in all notes: ";
	public static final String NUM_CHARS = "Total Number of characters in all notes: ";
	public static final String LONG_NOTE_WORD = "The most words in a single note: ";
	public static final String LONG_NOTE_CHAR = "The most characters in a single note: ";
	public static final String AVG_NOTE_WORD = "The average number of words in a note: ";
	public static final String AVG_NOTE_CHAR = "The average number of chars in a note: ";
	public static final String LIST_100_WORDS = "The 100 most common words:";
}
